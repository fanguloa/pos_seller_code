odoo.define('pos_seller_code.pos', function (require) {
    "use strict";

    var rpc = require('web.rpc');
    var screens = require('point_of_sale.screens');
    var gui = require('point_of_sale.gui');
    var ScreenWidget = screens.ScreenWidget;
    var core = require('web.core');
    var QWeb = core.qweb;
    var _t = core._t;
    var PopupWidget = require('point_of_sale.popups');

    var SellerCodePopUpWidget = PopupWidget.extend({
        template: 'SellerCodePopUpWidget',
        click_confirm: function () {
            var self = this;
            var order_lines = this.pos.get_order().get_orderlines();
            var order = this.pos.get_order();
            var client = order.get_client();
            var client_id = false;
            var seller_code = this.$('.seller_code').val();

            // almacenamos en un array sólo los códigos válidos desde los usuarios
            var valid_codes = [];
            for (let j = 0; j < this.pos.users.length; j++) {
                const element = this.pos.users[j];
                if (element.pos_seller_code) {
                    valid_codes.push(element.pos_seller_code);
                } 
            };

            if (client) {
                client_id = client['id']
            };
            
            if (order_lines.length !== 0) {
                // si el codigo ingresado esta en el array de codigos validos
                if (valid_codes.indexOf(seller_code) >= 0) {
                    var lines = [];
                    for (var i = 0, len = order_lines.length; i < len; i++) {
                        var discount = 0;
                        if (order_lines[i]['discount']) {
                            discount = order_lines[i]['discount']
                        }
                        lines.push({
                            'product_id': order_lines[i]['product']['id'],
                            'qty': order_lines[i]['quantity'],
                            'price': order_lines[i]['price'],
                            'discount': discount
                        });
                    };
                    var args = {
                        partner_id: client_id,
                        pos_ref: order['name'],
                        lines: lines,
                        seller_code: seller_code
                    };

                    self.create_sale_ticket(args);
                    order.destroy({ reason: "abandon" });

                } else {
                    self.gui.show_popup('error', {
                        'title': _t('Error en Código de Vendedor'),
                        'body': _t('Código Inválido')
                    });

                }
                
            } else {
                // si no hya lineas en el ticket
                self.gui.show_popup('error', {
                    'title': _t('Error en Orden de Venta'),
                    'body': _t('Debes ingresar algunas líneas de pedido para esta orden!!')
                });
            }

        },
        // funcion crea pos.order transitoria
        create_sale_ticket: function (args) {
            var self = this;
            rpc.query({
                model: 'pos.order',
                method: 'create_ticket_from_ui',
                args: [args]
            }).then(function (res) {
                if (res) {
                    self.click_cancel();
                }
            });

        },

        click_cancel: function () {
            this.gui.close_popup();
        },

    });

    gui.define_popup({ name: 'seller_code', widget: SellerCodePopUpWidget });


    var CreateSaleTicketButton = screens.ActionButtonWidget.extend({
        template: 'CreateSaleTicketButton',
        button_click: function () {
            var self = this;
            this.gui.show_popup('seller_code');
            
        },
    });

    screens.define_action_button({
        'name': 'pos_sale_order',
        'widget': CreateSaleTicketButton,
        'condition': function () {
            return this.pos.config.seller_session;
        }
    });

    var ListSaleTicketButton = screens.ActionButtonWidget.extend({
        template: 'ListSaleTicketButton',
        button_click: function () {
            this.gui.show_screen('ticket_sale_list');
        }
    });

    screens.define_action_button({
        'name': 'pos_ticket_sale_list',
        'widget': ListSaleTicketButton,
        'condition': function () {
            return !this.pos.config.seller_session;
        }
    });

    var ListSaleTicketScreenWidget = ScreenWidget.extend({
        template: 'ListSaleTicketScreenWidget',
        back_screen: 'product',

        init: function (parent, options) {
            var self = this;
            this._super(parent, options);
        },

        show: function () {
            var self = this;
            this._super();
            
            this.renderElement();
            var order = this.pos.get_order();
            this.$('.back').click(function () {
                self.gui.back();
            });
            this.$('.searchbox input').focus();
            // traemos todas las ordenes del backend
            self.rpc_so();

            // Acción del botón Confirmar en cada linea de ticket
            this.$('.sale-ticket-list-contents').delegate('.ticket-line .confirm_ticket', 'click', function (event) {
                var ticket_id = parseInt($(this.parentElement.parentElement).data('id'));
                var partner = parseInt($(this.parentElement.parentElement).data('partner'));
                var el = $(this.parentElement.parentElement);
                var seller_code = $(this.parentElement.parentElement).data('seller_code');
                order.seller_code = seller_code;
                self.rpc_solines(event, el, ticket_id, partner);
            });
            // TODO
            // var search_timeout = null;

            // if (this.pos.config.iface_vkeyboard && this.chrome.widget.keyboard) {
            //     this.chrome.widget.keyboard.connect(this.$('.searchbox input'));
            // }

            // this.$('.searchbox input').on('keyup', function (event) {
            //     clearTimeout(search_timeout);
            //     var query = this.value;
            //     search_timeout = setTimeout(function () {
            //         self.perform_search(query, event.which === 13);
            //     }, 70);
            // });

            // this.$('.searchbox .search-clear').click(function () {
            //     self.clear_search();
            // });


        },

        // funcion para traer ordenes desde backend
        rpc_so: function () {
            var self = this;
            rpc.query({
                model: 'pos.order',
                method: 'search_orders',
                args: []
            }).then(function (res) {
                if (res.length > 0) {
                    // renderizamos la lista de ordenes
                    self.render_list(res);
                }
            });
        },

        render_list: function (res) {
            var contents = this.$el[0].querySelector('.sale-ticket-list-contents');
            contents.innerHTML = "";
            for (var i = 0, len = Math.min(res.length, 1000); i < len; i++) {
                const ticket = res[i];
                var ticket_line_html = QWeb.render('ListSaleTicketLine', { widget: this, ticket: ticket });
                var ticket_line = document.createElement('tbody');
                ticket_line.innerHTML = ticket_line_html;
                ticket_line = ticket_line.childNodes[1];
                contents.appendChild(ticket_line);
            }
        },

        rpc_solines: function (event, el, ticket_id, partner) {
            var self = this;
            rpc.query({
                model: 'pos.order.line',
                method: 'search_order_line',
                args: [ticket_id]
            }).then(function (res) {
                self.line_select(event, el, ticket_id, partner, res);
                return res;

            });
        },

        

        line_select: function (event, $line, ticket_id, partner_id, res) {
            var self = this;
            var order = this.pos.get_order();
            for (var i = 0, len = res.length; i < len; i++) {
                var prod_id = this.pos.db.get_product_by_id(res[i]['product']);
                // agrego el producto al ticket desde la linea de la orden transitoria
                order.add_product(prod_id, { quantity: res[i]['qty'], price: res[i]['price'], discount: res[i]['discount'] });

            }
            // agregar el ticket de referencia a la nueva orden para elimniar despues por rpc
            if (ticket_id) {
                order.ticket_id = ticket_id;
            }

            if (partner_id) {
                var partner = this.pos.db.get_partner_by_id(partner_id);
                order.set_client(partner);
            }
            this.gui.show_screen('products');
            // agrego el codigo del vendedor desde la orden transitoria 
            
            

        },
        
        // TODO 
        // perform_search: function (query) {
        //     var self = this;
        //     var domain = [['ean13', '=', query]];
        //     self.rpc_so(domain);
        // },
        // clear_search: function () {
        //     var self = this;
        //     var domain = [['state', '=', 'draft']];
        //     self.rpc_so(domain);
        //     this.$('.searchbox input')[0].value = '';
        //     this.$('.searchbox input').focus();
        // }
    });

    gui.define_screen({ name: 'ticket_sale_list', widget: ListSaleTicketScreenWidget });

    screens.PaymentScreenWidget.include({
        // sobreecribo esta funcion para eliminar del backend la orden temporal
        validate_order: function (force_validation) {
            var self = this;
            var order = this.pos.get_order();
            var ticket_id = order.ticket_id;
            if (this.order_is_valid(force_validation)) {
                

                self.unlink_temp_order(ticket_id)
                this.finalize_validation();
            }
        },

        unlink_temp_order: function (ticket_id) {
            rpc.query({
                model: 'pos.order',
                method: 'unlink_temp_order',
                args: [ticket_id]
            }).then(function (res) {
                if (res.length > 0) {

                }
            });

        },

    });
})