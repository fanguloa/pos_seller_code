odoo.define('pos_seller_code.access_right', function (require) {
    "use strict";

    var screens = require('point_of_sale.screens');
    // var chrome = require('point_of_sale.chrome');
    // var models = require('point_of_sale.models');
    var gui = require('point_of_sale.gui');
    var core = require('web.core');
    var _t = core._t;

    // Nueva Funcion 'display_access_right' 
    gui.Gui.prototype.display_access_right = function () {
        if (this.pos.config.seller_session) {
            // Desactivar botón de pagos
            console.log('dscativar el botn de pago');
            $(".button.pay").addClass('pos-disabled-mode');
        } else {
            $(".button.pay").removeClass('pos-disabled-mode');
        }
    };
    screens.ActionpadWidget.include({

        /**
         * Bloquea el botón 'Pagar' 
         */
        renderElement: function () {
            var self = this;
            this._super();
            this.gui.display_access_right();
            var button_pay_click_handler = $._data(
                this.$el.find(".button.pay")[0], "events").click[0].handler;
            this.$('.pay').off('click').click(function () {
                if (self.pos.config.seller_session) {
                    self.gui.show_popup('error', {
                        'title': _t('Pagar - Función no Autorizada'),
                        'body': _t('Pregunte al administrador.'),
                    });
                } else {
                    button_pay_click_handler();
                }
            });
        },
    });

});
