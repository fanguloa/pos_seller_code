odoo.define('pos_seller_code.models', function (require) {
    var models = require('point_of_sale.models');

    models.load_fields('res.users', 'pos_seller_code');

    var _super_Order = models.Order.prototype;
    models.Order = models.Order.extend({
        initialize: function (attributes, options) {
            _super_Order.initialize.apply(this, arguments);
        },

        init_from_JSON: function (json) {
            var res = _super_Order.init_from_JSON.apply(this, arguments);
            if (json.seller_code) {
                this.seller_code = json.seller_code;
            }
            return res;
        },
        export_as_JSON: function () {
            var json = _super_Order.export_as_JSON.apply(this, arguments);
            // agregaoms seller_code al json que es el que se pasa en la funcion _save_to_server
            // para asi tenerlo en la data de create_from_ui en python :)
            if (this.seller_code) {
                json.seller_code = this.seller_code;
            }

            return json;
        }
    });
    

});