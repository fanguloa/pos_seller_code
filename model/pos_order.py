from odoo import api, fields, models, _
from functools import partial
import logging

_logger = logging.getLogger(__name__)


class PosOrder(models.Model):
    _inherit = 'pos.order'
    _description = 'Seller Code'

    seller_code = fields.Char('C. Vendedor')

    @api.model
    def create_ticket_from_ui(self, args):
        order_line_obj = self.env['pos.order.line']
        order_vals = {
            'pos_reference': args.get('pos_ref'),
            'partner_id': args.get('partner_id'),
            'state': 'draft',
            'seller_code': args.get('seller_code'),
            'amount_tax': 0,
            'amount_total': 0,
            'amount_paid': 0,
            'amount_return': 0
        }

        ##creamos una orden del pos transitoria, se eliminará cuando confirmemos la orden en la caja
        order_id = self.create(order_vals)
        ## Actualizamos el usuario de la orden como una manera de linkear el codigo con el usuario
        if args.get('seller_code'):
            user_id = self.env['res.users'].search([('pos_seller_code', '=',
                                                     args.get('seller_code'))])
            order_id.update({'user_id': user_id.id})
        ## Creamos las lineas de la orden
        for l in args.get('lines'):
            line_vals = {
                'name': order_id.name,
                'product_id': l.get('product_id'),
                'price_unit': l.get('price'),
                'qty': l.get('qty'),
                'order_id': order_id.id,
                'price_subtotal': 0,
                'price_subtotal_incl': 0,
                'discount': l.get('discount')
            }
            order_line_obj.create(line_vals)
        return True

    @api.model
    def search_orders(self):
        domain = [('state', '=', 'draft')]
        order_obj = self.search(domain)
        orders = []
        for order in order_obj:
            pos_order = {
                'id': order.id,
                'name': order.name,
                'partner_id': order.partner_id.id,
                'partner_name': order.partner_id.name,
                'date_order': order.date_order,
                'amount_total': order.amount_total,
                'seller_name': order.user_id.partner_id.name,
                'seller_code': order.seller_code
            }
            orders.append(pos_order)

        return orders

    @api.model
    def unlink_temp_order(self, ticket_id):
        order_id = self.browse(int(ticket_id))
        order_id.unlink()
        return True

    @api.model
    def _order_fields(self, ui_order):
        # Heredamos este metodo que es el que entrega los campos de la orden pos para crearlos
        # Con esto actualizamos el vendedor respecto del codigo del vendedor
        order_fields = super(PosOrder, self)._order_fields(ui_order)
        seller_code = ui_order['seller_code']
        user_id = self.env['res.users'].search([('pos_seller_code', '=', seller_code)])
        if not user_id:
            user_id = ui_order['user_id'] or False
        order_fields.update({
            'user_id': user_id.id,
            'seller_code': seller_code
        })

        return order_fields

class PosOrderLines(models.Model):
    _inherit = 'pos.order.line'

    @api.model
    def search_order_line(self, ticket_id):
        order_line_obj = self.search([('order_id', '=', int(ticket_id))])
        order_lines = []
        for line in order_line_obj:
            lines = {
                'id': line.id,
                'product': line.product_id.id,
                'qty': line.qty,
                'price': line.price_unit,
                'discount': line.discount
            }
            order_lines.append(lines)

        return order_lines

class PosSession(models.Model):
    _inherit = 'pos.session'

    ## Sobre escribo este metodo en el cierre de la sesion para saltarse las ordenes en estado borrador
    ## Si es que la sesion es de vendedor solor para no tener errores  si es que llegara a cerrar esa 'Caja'
    ## Contablemente no denderia pasar nada ya eu este tipo de sesiones no pagan 
    #TODO mejorar o hacer una herencia vs sobreescribir???

    def _confirm_orders(self):
        for session in self:
            seller_session = session.config_id.seller_session
            company_id = session.config_id.journal_id.company_id.id
            orders = session.order_ids.filtered(lambda order: order.state == 'paid')
            journal_id = self.env['ir.config_parameter'].sudo().get_param(
                'pos.closing.journal_id_%s' % company_id, default=session.config_id.journal_id.id)
            if not journal_id:
                raise UserError(_("You have to set a Sale Journal for the POS:%s") % (session.config_id.name,))

            move = self.env['pos.order'].with_context(force_company=company_id)._create_account_move(session.start_at, session.name, int(journal_id), company_id)
            orders.with_context(force_company=company_id)._create_account_move_line(session, move)
            for order in session.order_ids.filtered(lambda o: o.state not in ['done', 'invoiced']):
                if seller_session:
                    return
                if order.state not in ('paid'):
                    raise UserError(
                        _("You cannot confirm all orders of this session, because they have not the 'paid' status.\n"
                          "{reference} is in state {state}, total amount: {total}, paid: {paid}").format(
                            reference=order.pos_reference or order.name,
                            state=order.state,
                            total=order.amount_total,
                            paid=order.amount_paid,
                        ))
                order.action_pos_order_done()
            orders_to_reconcile = session.order_ids._filtered_for_reconciliation()
            orders_to_reconcile.sudo()._reconcile_payments()
