from odoo import api, fields, models, _
import logging

_logger = logging.getLogger(__name__)


class PosConfig(models.Model):
    _inherit = "pos.config"

    seller_session = fields.Boolean('Sesión Mesón', help='Si está activa no se podrán hacer pagos desde esta sesión,'
                                                         'solo generar ordenes para caja', default=0)