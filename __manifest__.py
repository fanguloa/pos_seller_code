# -*- coding: utf-8 -*-
{
    'name': "Código de Vendedor en POS",
    'category': 'Point of Sale',
    'sequence': 2,

    'summary': """Código de Vendedor para Punto de Venta. Permite varias sesiones deVentas y sesiones de Caja.""",

    'description': """
        Código de Vendedor para Punto de Venta. Permite varias sesiones deVentas y sesiones de Caja.
    """,

    'author': "Felipe Angulo A.",
    'website': "http://www.mobilize.cl",
    'version': '1.0.1',

    # any module necessary for this one to work correctly
    'depends': ['point_of_sale', 'base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/pos_config.xml',
        'views/pos_order.xml',
        'views/res_user_views.xml',
        'templates/assets.xml',
    ],
    'qweb': [
        'static/src/xml/pos.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}